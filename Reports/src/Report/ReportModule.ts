import { Module } from '@nestjs/common';
import { ReportController } from './Controller/ReportController';
import { OrderModule } from '../Order/OrderModule';
import { BestCustomer } from './Service/BestCustomer';
import { BestProduct } from './Service/BestProduct';

@Module({
  imports: [OrderModule],
  controllers: [ReportController],
  providers: [BestCustomer, BestProduct],
})
export class ReportModule {}
