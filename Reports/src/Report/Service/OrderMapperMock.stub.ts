import { OrderMapper } from 'src/Order/Service/OrderMapper';

export const mockData = {
  async getOrdersByDate(date) {
    return [
      {
        customerName: 'Jane Doe',
        number: '2019/08/1',
        createdAt: '2019-08-08',
        customer: { firstName: 'Jane', lastName: 'Doe' },
        products: [{ name: 'Black sport shoes', price: 110 }],
      },
      {
        customerName: 'John Doe',
        number: '2019/08/2',
        createdAt: '2019-08-08',
        customer: { firstName: 'John', lastName: 'Doe' },
        products: [{ name: 'Cotton t-shirt XL', price: 25.75 }],
      },
      {
        customerName: 'John Doe',
        number: '2019/08/3',
        createdAt: '2019-08-08',
        customer: { firstName: 'John', lastName: 'Doe' },
        products: [{ name: 'Blue jeans', price: 55.99 }],
      },
    ];
  },
} as OrderMapper;
