import { Injectable, Inject } from '@nestjs/common';
import { OrderMapper } from '../../Order/Service/OrderMapper';
import { IBestBuyers, IBestSellers } from '../Model/IReports';
import { BestSeller } from '../Model/BestSeller';

@Injectable()
export class BestProduct {
  @Inject() orderMapper: OrderMapper;

  async bestProductForDate(date: string): Promise<IBestSellers[]> {
    const ordersByDate = await this.orderMapper.getOrdersByDate(date);
    return ordersByDate
      .map(({ products }) => products)
      .reduce((acc, next) => acc.concat(next), [])
      .reduce((result: IBestSellers[], product) => {
        const productIndex = result.findIndex(
          p => p.productName === product.name,
        );
        if (productIndex === -1) {
          return [...result, BestSeller.fromProduct(product)];
        } else {
          result[productIndex] = BestSeller.mergeWithProduct(
            result[productIndex],
            product,
          );
        }
        return result;
      }, [])
      .sort((a, b) => b.totalPrice - a.totalPrice);
  }
}
