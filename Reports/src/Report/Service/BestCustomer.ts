import { Injectable, Inject } from '@nestjs/common';
import { OrderMapper } from '../../Order/Service/OrderMapper';
import { IBestBuyers } from '../Model/IReports';
import { BestBuyer } from '../Model/BestBuyer';

@Injectable()
export class BestCustomer {
  @Inject() orderMapper: OrderMapper;

  async bestBuyerForDate(date: string): Promise<IBestBuyers[]> {
    const ordersByDate = await this.orderMapper.getOrdersByDate(date);
    return ordersByDate
      .reduce((result: IBestBuyers[], order) => {
        const customerIndex = result.findIndex(
          b => b.customerName === order.customerName,
        );
        if (customerIndex === -1) {
          return [...result, BestBuyer.fromOrder(order)];
        } else {
          result[customerIndex] = BestBuyer.mergeWithOrder(
            result[customerIndex],
            order,
          );
        }
        return result;
      }, [])
      .sort((a, b) => b.totalPrice - a.totalPrice);
  }
}
