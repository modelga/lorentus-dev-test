import { Test } from '@nestjs/testing';
import { OrderMapper } from '../../Order/Service/OrderMapper';
import { mockData } from './OrderMapperMock.stub';
import { BestProduct } from './BestProduct';

describe('BestCustomer', () => {
  let bestProduct: BestProduct;
  beforeEach(async () => {
    const module = await Test.createTestingModule({
      controllers: [],
      providers: [BestProduct, { provide: OrderMapper, useValue: mockData }],
    }).compile();
    bestProduct = module.get<BestProduct>(BestProduct);
  });
  test('should get best customer', async () => {
    const result = await bestProduct.bestProductForDate('2019-08-08');

    expect(result[0]).toMatchObject({
      productName: 'Black sport shoes',
      quantity: 1,
      totalPrice: 110,
    });
    expect(result[1]).toMatchObject({
      productName: 'Blue jeans',
      quantity: 1,
      totalPrice: 55.99,
    });
    expect(result[2]).toMatchObject({
      productName: 'Cotton t-shirt XL',
      quantity: 1,
      totalPrice: 25.75,
    });
  });
});
