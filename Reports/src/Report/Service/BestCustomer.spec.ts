import { Test } from '@nestjs/testing';
import { BestCustomer } from './BestCustomer';
import { OrderMapper } from '../../Order/Service/OrderMapper';
import { mockData } from './OrderMapperMock.stub';

describe('BestCustomer', () => {
  let bestCustomer: BestCustomer;
  beforeEach(async () => {
    const module = await Test.createTestingModule({
      controllers: [],
      providers: [BestCustomer, { provide: OrderMapper, useValue: mockData }],
    }).compile();
    bestCustomer = module.get<BestCustomer>(BestCustomer);
  });
  test('should get best customer', async () => {
    const result = await bestCustomer.bestBuyerForDate('2019-08-08');

    expect(result[0]).toMatchObject({
      customerName: 'Jane Doe',
      totalPrice: 110,
    });
    expect(result[1]).toMatchObject({
      customerName: 'John Doe',
      totalPrice: 55.99 + 25.75,
    });
  });
});
