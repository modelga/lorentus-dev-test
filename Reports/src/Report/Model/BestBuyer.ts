import { Order } from 'src/Order/Model/Order';
import { IBestBuyers } from './IReports';
export class BestBuyer implements IBestBuyers {
  constructor(
    public readonly customerName: string,
    public readonly totalPrice: number,
  ) {}

  static mergeWithOrder(bestBuyer: IBestBuyers, order: Order): IBestBuyers {
    const { totalPrice } = BestBuyer.fromOrder(order);
    return new BestBuyer(
      bestBuyer.customerName,
      bestBuyer.totalPrice + totalPrice,
    );
  }

  static fromOrder(order: Order): IBestBuyers {
    const totalPrice = order.products.reduce(
      (result, next) => result + next.price,
      0,
    );
    return new BestBuyer(order.customerName, totalPrice);
  }
}
