import { BestBuyer } from './BestBuyer';
import { Order } from '../../Order/Model/Order';
import { Customer } from '../../Order/Model/Customer';
import { Product } from '../../Order/Model/Product';

const order = new Order('1', '2019-08-08', new Customer('John', 'Doe'), [
  new Product('T-Shirt', 1),
  new Product('Backpack', 10),
  new Product('Rare T-Shirt', 5),
]);

describe('BestBuyer', () => {
  test('should be able to create IBestBuyer from Order', () => {
    const result = BestBuyer.fromOrder(order);
    expect(result.customerName).toBe('John Doe');
    expect(result.totalPrice).toBe(16);
  });

  test('should be able to merge IBestBuyer with another Order', () => {
    const aBuyer = BestBuyer.fromOrder(order);
    const result = BestBuyer.mergeWithOrder(aBuyer, order);
    expect(result.customerName).toBe('John Doe');
    expect(result.totalPrice).toBe(32);
  });
});
