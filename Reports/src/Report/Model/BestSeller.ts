import { Product } from '../../Order/Model/Product';
import { IBestSellers } from './IReports';
export class BestSeller implements IBestSellers {
  constructor(
    public readonly productName: string,
    public readonly quantity: number,
    public readonly totalPrice: number,
  ) {}
  static fromProduct(product: Product): IBestSellers {
    return new BestSeller(product.name, 1, product.price);
  }
  static mergeWithProduct(
    bestSeller: IBestSellers,
    product: Product,
  ): IBestSellers {
    return new BestSeller(
      product.name,
      bestSeller.quantity + 1,
      bestSeller.totalPrice + product.price,
    );
  }
}
