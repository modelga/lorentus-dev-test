import { BestSeller } from './BestSeller';
import { Product } from '../../Order/Model/Product';
import { IBestSellers } from './IReports';

const tShirt = new Product('T-Shirt', 8);

describe('BestBuyer', () => {
  test('should be able to create IBestSeller from Product', () => {
    const result = BestSeller.fromProduct(tShirt);
    expect(result.productName).toBe('T-Shirt');
    expect(result.quantity).toBe(1);
    expect(result.totalPrice).toBe(8);
  });

  test('should be able to merge IBestBuyer with another Order', () => {
    const bestSeller = BestSeller.fromProduct(tShirt);
    let result = BestSeller.mergeWithProduct(bestSeller, tShirt);
    result = BestSeller.mergeWithProduct(result, tShirt);
    result = BestSeller.mergeWithProduct(result, tShirt);

    expect(result.productName).toBe('T-Shirt');
    expect(result.quantity).toBe(4);
    expect(result.totalPrice).toBe(4 * 8);
  });
});
