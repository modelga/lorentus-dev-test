import { Controller, Get, Param, Inject } from '@nestjs/common';
import { BestCustomer } from '../Service/BestCustomer';
import { BestProduct } from '../Service/BestProduct';

@Controller()
export class ReportController {
  @Inject() bestCustomer: BestCustomer;
  @Inject() BestProduct: BestProduct;
  @Get('/report/products/:date')
  bestSellers(@Param('date') date: string) {
    return this.BestProduct.bestProductForDate(date);
  }

  @Get('/report/customer/:date')
  bestBuyers(@Param('date') date: string) {
    return this.bestCustomer.bestBuyerForDate(date);
  }
}
