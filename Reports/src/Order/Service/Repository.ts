import { Injectable } from '@nestjs/common';

export interface Order {
  number: string;
  createdAt: string;
  customer: number;
  products: number[];
}

export interface Product {
  id: number;
  name: string;
  price: number;
}
export interface Customer {
  id: number;
  firstName: string;
  lastName: string;
}

/**
 * Data layer - mocked
 */
@Injectable()
export class Repository {
  fetchOrders(): Promise<Order[]> {
    return new Promise(resolve => resolve(require('../Resources/Data/orders')));
  }

  fetchProducts(): Promise<Product[]> {
    return new Promise(resolve =>
      resolve(require('../Resources/Data/products')),
    );
  }

  fetchCustomers(): Promise<Customer[]> {
    return new Promise(resolve =>
      resolve(require('../Resources/Data/customers')),
    );
  }
}
