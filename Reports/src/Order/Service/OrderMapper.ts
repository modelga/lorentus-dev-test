import { Injectable, Inject } from '@nestjs/common';
import { Repository } from './Repository';
import { Order } from '../Model/Order';

function groupById<T extends { id: any }>(array: T[]): { [key: string]: T } {
  return array.reduce((acc, next) => {
    return { ...acc, [next.id]: next };
  }, {});
}

@Injectable()
export class OrderMapper {
  @Inject() repository: Repository;
  private async getAndMapOrders() {
    const [customers, products, orders] = await Promise.all([
      this.repository.fetchCustomers().then(groupById),
      this.repository.fetchProducts().then(groupById),
      this.repository.fetchOrders(),
    ]);
    return orders.map(Order.fromRepository(customers, products));
  }
  async getOrdersByDate(date: string): Promise<Order[]> {
    const mappedOrders = await this.getAndMapOrders();
    return mappedOrders.filter(({ createdAt }) => createdAt === date);
  }
}
