import { OrderMapper } from './OrderMapper';
import { Test, TestingModule } from '@nestjs/testing';
import { Repository } from './Repository';

describe('OrderMapper', () => {
  let orderMapper: OrderMapper;
  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [OrderMapper, Repository],
    }).compile();

    orderMapper = module.get<OrderMapper>(OrderMapper);
  });
  test('should get orders only for 2019-08-07', async () => {
    const result = await orderMapper.getOrdersByDate('2019-08-07');
    expect(result.length).toBeGreaterThan(0);
    result.forEach(({ createdAt }) => expect(createdAt).toBe('2019-08-07'));
  });
});
