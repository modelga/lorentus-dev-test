import { Product as RepositoryProduct } from '../Service/Repository';

export class Product {
  constructor(public readonly name: string, public readonly price: number) {}
  static fromRepository(product: RepositoryProduct) {
    return new Product(product.name, product.price);
  }
}
