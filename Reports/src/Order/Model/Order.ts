import { Product } from './Product';
import { Customer } from './Customer';
import {
  Order as RepositoryOrder,
  Customer as RepositoryCustomer,
  Product as RepositoryProduct,
} from '../Service/Repository';

export class Order {
  constructor(
    public readonly number: string,
    public readonly createdAt: string,
    public readonly customer: Customer,
    public readonly products: Product[],
  ) {}
  get customerName() {
    return `${this.customer.firstName} ${this.customer.lastName}`;
  }
  static fromRepository(
    customers: { [key: string]: RepositoryCustomer },
    products: { [key: string]: RepositoryProduct },
  ): (order: RepositoryOrder) => Order {
    return order =>
      new Order(
        order.number,
        order.createdAt,
        Customer.fromRepository(customers[order.customer]),
        order.products.map(id => products[id]).map(Product.fromRepository),
      );
  }
}
