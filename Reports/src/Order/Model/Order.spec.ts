import { Order } from './Order';

describe('Order', () => {
  const customers = {
    1: { id: 1, firstName: 'John', lastName: 'Doe' },
    2: { id: 2, firstName: 'Jane', lastName: 'Doe' },
  };
  const products = {
    1: { id: 1, name: 'T-Shirt', price: 1 },
    2: { id: 2, name: 'Pants', price: 10 },
  };
  const fromRepository = Order.fromRepository(customers, products);
  test('should create object using repository data', () => {
    const order = {
      number: '1',
      createdAt: '2019-01-01',
      customer: 1,
      products: [1, 2, 1],
    };
    const result = fromRepository(order);
    expect(result).toMatchObject({
      number: '1',
      createdAt: '2019-01-01',
      customer: { firstName: 'John', lastName: 'Doe' },
      products: [
        { name: 'T-Shirt', price: 1 },
        { name: 'Pants', price: 10 },
        { name: 'T-Shirt', price: 1 },
      ],
    });
  });
  test('should get customerName', () => {
    const order = {
      number: '1',
      createdAt: '2019-01-01',
      customer: 1,
      products: [1, 2, 1],
    };
    const result = fromRepository(order);
    expect(result.customerName).toBe('John Doe');
  });
});
