import { Customer as RepositoryCustomer } from '../Service/Repository';
export class Customer {
  constructor(
    public readonly firstName: string,
    public readonly lastName: string,
  ) {}
  static fromRepository(customer: RepositoryCustomer) {
    return new Customer(customer.firstName, customer.lastName);
  }
}
