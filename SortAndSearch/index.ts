import { ASort } from "./ASort";
import { BSort } from "./BSort";

const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];

const sorted = [...unsorted].sort((a, b) => a - b);
[ASort, BSort]
  .map(algorithm => new algorithm([...unsorted]))
  .map(algorithm =>
    algorithm.sort().reduce((isSorted, value, index) => {
      return isSorted && value == sorted[index];
    }, true)
  )
  .forEach((isSorted, index) => {
    console.log(
      `${isSorted ? "✅" : "❌"}  Algorithm ${index + 1} has${
        !isSorted ? " not" : ""
      } sorted an array`
    );
  });

import { BSearch } from "./BSearch";

const elementsToFind = [1, 5, 13, 27, 77];

const lookups: Array<[number, number]> = [[1, 0], [13, 2], [77, 4], [4, -1]];

lookups.forEach(([lookup, expectedResult]) => {
  const index = BSearch.getInstance().find(elementsToFind, lookup);
  const asExpected = index === expectedResult;
  const operations = BSearch.getInstance().numberOfOperations;
  console.log(
    `${asExpected ? "✅" : "❌"}  Index of ${lookup} has been ` +
      `${index === -1 ? "not " : ""}found as ` +
      `${!asExpected ? "not " : ""}expected ` +
      `(${operations} operations)`
  );
});
