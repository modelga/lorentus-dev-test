export class BSearch {
  static instance: BSearch;
  private operations: number = 0;
  find(bTree: Array<Number>, lookup: Number): Number {
    this.operations++;
    let left = 0;
    let right = bTree.length - 1;
    while (left <= right) {
      this.operations++;
      const middleIndex = left + Math.floor((right - left) / 2);
      const middleValue = bTree[middleIndex];
      if (middleValue === lookup) {
        return middleIndex;
      }
      if (middleValue > lookup) {
        right = middleIndex - 1;
      } else {
        left = middleIndex + 1;
      }
    }
    return -1;
  }

  static getInstance(): BSearch {
    return BSearch.instance || (BSearch.instance = new BSearch());
  }
  get numberOfOperations(): Number {
    return this.operations;
  }
}
