export class ASort {
  constructor(private arrayToSort: Array<Number>) {}
  sort(): Array<Number> {
    let swapped = false;
    let sortedElements = 1;
    while (swapped || sortedElements !== this.arrayToSort.length) {
      swapped = false;
      for (let i = 0; i < this.arrayToSort.length - sortedElements; i++) {
        if (this.arrayToSort[i] > this.arrayToSort[i + 1]) {
          [this.arrayToSort[i], this.arrayToSort[i + 1]] = [
            this.arrayToSort[i + 1],
            this.arrayToSort[i]
          ];
          swapped = true;
        }
      }
      sortedElements++;
    }
    return this.arrayToSort;
  }
}
