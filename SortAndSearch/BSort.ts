export class BSort {
  constructor(private arrayToSort: Array<Number>) {}
  private swap(left: number, right: number): void {
    [this.arrayToSort[left], this.arrayToSort[right]] = [
      this.arrayToSort[right],
      this.arrayToSort[left]
    ];
  }
  private partitionArray(fromIndex: number, toIndex: number): number {
    const pivot = this.arrayToSort[toIndex];
    let partitionIndex = fromIndex;
    for (let currentIndex = fromIndex; currentIndex < toIndex; currentIndex++) {
      if (this.arrayToSort[currentIndex] < pivot) {
        this.swap(partitionIndex++, currentIndex);
      }
    }
    this.swap(partitionIndex, toIndex);
    return partitionIndex;
  }

  sort(
    fromIndex: number = 0,
    toIndex: number = this.arrayToSort.length - 1
  ): Array<Number> {
    if (fromIndex < toIndex) {
      const partitionIndex = this.partitionArray(fromIndex, toIndex);
      this.sort(fromIndex, partitionIndex - 1);
      this.sort(partitionIndex + 1, toIndex);
    }
    return this.arrayToSort;
  }
}
